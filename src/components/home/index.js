import React, {useEffect, useState} from "react";
import indexStyle from './indexStyle.module.css'
import ModalPopUp from "../modal/popup";
import ChatList from "../list/chat-list";
import ChatDetail from "../chat-detail/detail";

const Index = ()=>{
    const [username, setUsername] = useState('')
    const [chat,setChat] = useState({})

    const assignUsername = (data) =>{
        setUsername(data)
    }
    const chatDetail = (chat)=>{
        setChat(chat)
    }

    return(
        <div>
            <ModalPopUp assignUsername={assignUsername}/>    
            {
            username &&(
                <div className={indexStyle.bg}>
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12 col-md-4 col-lg-4">
                            <ChatList chatDetail={chatDetail} username={username}/>
                        </div>
                        <div className="col-sm-12 col-md-8 col-lg-8">
                            <div className={indexStyle.detail}>
                                <ChatDetail chat={chat} />

                            </div>

                        </div>
                    </div>    
                </div>
                </div>

            )  
            }
        </div>
    )
}

export default Index