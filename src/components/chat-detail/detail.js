import React, {useEffect, useState} from "react";
import detailStyle from '../chat-detail/detail.module.css'
import NewChat from "../new-chat";



const ChatDetail = ({chat})=>{

    useEffect(()=>{
    }, [])
    console.log(chat, 'ji')
    if(chat?.username){
        return(
            <div className={detailStyle.body}>
               
                <div className="container">
                <div className="row">
                    <div className="col-10">
                        <div className={detailStyle.info}>
                            <h3>{chat?.username}</h3>
    
                        </div>
                        <div className={detailStyle.chats}>
                            {
                                chat?.chats?.map((chat)=>{
                                    return <div>
                                        {chat}
                                    </div>
                                }
                                )
                            }
                        </div>
                        <div className={detailStyle.new}>
                            <NewChat/>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        )
    }
    
}

export default ChatDetail