import React, {useEffect, useState} from "react";
import ChatStyle from '../new-chat/chat.module.css'
import { SendFill } from "react-bootstrap-icons";


const NewChat = ()=>{
    const [newChat, setNewChat] = useState('')

    const onSubmit = ()=>{
        console.log(newChat)
    }

    return <div>
            <div className={ChatStyle.body}>
                <div className={ChatStyle.input}>
                    <input 
                         value={newChat} 
                         placeholder="type message here" 
                         className="form-control" 
                         onChange={(e)=>setNewChat(e.target.value)}
                    />
                    <button onClick={onSubmit}><SendFill/></button>
                </div>
            </div>
    </div>

}
export default NewChat