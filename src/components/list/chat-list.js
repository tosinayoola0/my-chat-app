import React, {useEffect, useState} from 'react'
import { useDispatch } from 'react-redux'
import  {displayChatList} from '../../slice/dataSlice'
import ListStyle from './chat-list.module.css'
import { Link } from 'react-router-dom'
import {PeopleFill, Ubuntu} from 'react-bootstrap-icons'
import {ChatSquareTextFill} from 'react-bootstrap-icons'



const ChatList = ({chatDetail, username})=> {
    const [chats, setChats] = useState([])
    const dispatch = useDispatch()



    useEffect(()=>{
        displayChats()
    }, [])

    function displayChats(){
        dispatch(displayChatList()).then((res)=>{
            console.log(res.payload)
            setChats(res.payload)
        })
    }
    
    const handldeClick = (event)=>{
        chatDetail(event)
    }
        
    return (
        <div className={ListStyle.body}> 
            <div className={ListStyle.chat_list}>
                <div className={ListStyle.icons}>
                    <div className={ListStyle.icon_group} >
                        <h4>welcome, {username}</h4>
                        <div className={ListStyle.group}>
                             <PeopleFill />
                        </div>
                        <div className={ListStyle.group}>
                          
                          <Ubuntu />

                        </div>
                        <div className={ListStyle.group}>
                          
                          <ChatSquareTextFill />

                        </div>
                    </div>
                </div>
                <ul>
                    {chats.map((chat)=>{
                        return <li key={chat.id}>
                                    <Link onClick={()=>handldeClick(chat)} className={ListStyle.link}>{chat.username}</Link>
                                </li>
                    })}
                </ul>
            </div>
            <div className={ListStyle.chat_detail}></div>

        </div>
    )
}
export default ChatList