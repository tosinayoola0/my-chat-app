import React, {useState, useEffect} from 'react'
import { Button, Modal } from 'react-bootstrap'


const ModalPopUp = ({assignUsername}) =>{

    const [isShow, setShow] = useState(false)
    const [username, setUsername] = useState('')
    const initModal = ()=>{
        console.log('hi')
        return setShow(true)
    }
    const onClose = () =>{
        console.log('close')
        setShow(false)}
    
    
    function onSubmit(){
        assignUsername(username)
        onClose()
    }

    useEffect(()=>{
        initModal()
    },[])

    return (
        <div>
           
            <Modal show={isShow}>
                <Modal.Title>Welcome to Neat Chat </Modal.Title>
                <Modal.Body>
                    <input 
                        placeholder='Enter your Username' 
                        className='form-control'
                        name='username'
                        type='text' 
                        value={username} 
                        onChange={e=>setUsername(e.target.value)}
                    />
                    <Button variant='success' onClick={onSubmit}>Login</Button>
                    <Button variant='danger' onClick={onClose}>Close</Button>

                </Modal.Body>
            </Modal>
        </div>
    )
}


export default ModalPopUp