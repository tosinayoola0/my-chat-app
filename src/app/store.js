import { configureStore } from '@reduxjs/toolkit';
import counterReducer from '../features/counter/counterSlice';
import dataReducer from '../slice/dataSlice'
import thunk from 'redux-thunk';

const reducers = {
  data: dataReducer
}


export const store = configureStore({
  reducer:reducers,
  devTools:true,
  middleware:[thunk]
});


